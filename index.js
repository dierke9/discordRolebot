const Discord = require('discord.js');
const bot = new Discord.Client();
const config = require('./config.json');
const _ = require('lodash');
const env = require('env2')('./.env');


let roles;
let mainChannel;
let guild;
let messages;

//Expandable using config file
bot.on('ready', () => {
    roles = config.roles;
    guild = bot.guilds.find('id', config.guildId);
    const guildChannels = guild.channels;
    mainChannel = config.mainChannel;
    const foundChannel = guildChannels.find('id', mainChannel);
    messages = config.messages;
    console.log('Bot ready');
})

bot.on('raw', async event => {
    if (event.t !== 'MESSAGE_REACTION_ADD' && event.t !== 'MESSAGE_REACTION_REMOVE') return;
    const { d: data } = event;
    if (!_.find(messages, message => { return message === data.message_id })) return;
    const channel = bot.channels.get(data.channel_id);
    const user = bot.users.get(data.user_id);
    const message = await channel.fetchMessage(data.message_id);
    const reaction = message.reactions.find(reaction => reaction.emoji.name === data.emoji.id || reaction.emoji.name === data.emoji.name);
    if (event.t === 'MESSAGE_REACTION_ADD') {
        handleReaction(reaction, user);
    } else {
        handleRemove(reaction, user);
    }
})

async function handleRemove(reaction, user) {
    const channelId = reaction.message.channel.id;
    const member = await guild.fetchMember(user);
    if (channelId === mainChannel) {
        const role = _.find(roles, ['emoji', reaction.emoji.name]);
        if (role) {
            await member.removeRole(role.mainRoleId);
            _.forEach(role.subRoles, async subRole => {
                await member.removeRole(subRole.id);
            })
        }
    } else {
        const mainRole = _.find(roles, ['channel', channelId]);
        if (mainRole) {
            const subRole = _.find(mainRole.subRoles, ['emoji', reaction.emoji.name]);
            if (subRole) {
                member.removeRole(subRole.id);
            }
        }
    }
}

async function handleReaction(reaction, user) {
    const channelId = reaction.message.channel.id;
    const member = await guild.fetchMember(user);
    if (channelId === mainChannel) {
        const role = _.find(roles, ['emoji', reaction.emoji.name]);
        if (role) {
            const discordRole = guild.roles.find('id', role.mainRoleId);
            if (discordRole) {
                await member.addRole(discordRole);
            } else {
                console.log("Role was not found");
            }
        }
    } else {
        const mainRole = _.find(roles, ['channel', channelId]);
        if (mainRole) {
            const subRole = _.find(mainRole.subRoles, ['emoji', reaction.emoji.name]);
            if (subRole) {
                const discordRole = guild.roles.find('id', subRole.id);
                if (discordRole) {
                    await member.addRole(discordRole);
                    await member.removeRole(mainRole.mainRoleId);
                } else {
                    console.log("Role was not found");
                }
            }
        }
    }
}

bot.login(process.env.login);